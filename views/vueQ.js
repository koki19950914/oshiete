new Vue({
    el: '#q-list',
    data: {
        q: [],
        title: '',
        content: ''
    },
    methods: {
        fetchQ() {
            axios.get('/fetchQ')
            .then(response => {
                if (response.status != 200) {
                    throw new Error('レスポンスエラー')
                } else {
                    var resultQ = response.data
                    // サーバから取得した商品情報をdataに設定する
                    this.q = resultQ
                }
            })
        }
    }
})