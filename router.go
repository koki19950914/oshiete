package main

import (
	"oshiete/q"
	"oshiete/sign"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql" //直接的な記述が無いが、インポートしたいものに対しては"_"を頭につける決まり
)

func main() {
	router := gin.Default()
	router.LoadHTMLGlob("./views/*.html")
	router.Static("/assets", "./views")
	sign.DbInit()

	router.GET("/signin", func(c *gin.Context) {
		c.HTML(200, "signin.html", gin.H{})
	})
	router.POST("/signin", sign.Signin)

	router.GET("/signup", func(c *gin.Context) {
		c.HTML(200, "signup.html", gin.H{})
	})
	router.POST("/signup", sign.Signup)

	router.GET("/home", func(c *gin.Context) {
		c.HTML(200, "home.html", gin.H{})
	})

	router.GET("/q", func(c *gin.Context) {
		c.HTML(200, "q.html", gin.H{})
	})
	router.POST("/q", q.Ask)
	// homeにて、検索ボタンを押すとDBから質問を取ってくる
	router.GET("/fetchQ", q.FetchQ)

	router.Run()
}
