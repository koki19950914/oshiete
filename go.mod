module oshiete

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.0.0-20210915214749-c084706c2272
)
