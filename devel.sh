#!/bin/bash

# install golang
GOLANG_VERSION=1.16.2	
sudo wget https://storage.googleapis.com/golang/go${GOLANG_VERSION}.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go${GOLANG_VERSION}.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin

echo "export GOPATH=$HOME/go" >>  $HOME/.bashrc
echo "export PATH=$GOPATH/bin:$PATH" >>	$HOME/.bashrc
source $HOME/.bashrc

rm go${GOLANG_VERSION}.linux-amd64.tar.gz -f

#install vscode
sudo apt install -y curl
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt install -y apt-transport-https
sudo apt update
sudo apt install code
rm microsoft.gpg

#install mysql




