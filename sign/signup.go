package sign

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	gorm.Model
	Username string
	Password string
	//Bio      string
}

func gormConnect() *gorm.DB {
	DBMS := "mysql"
	USER := "koki"
	PASS := "mayomayo"
	DBNAME := "user"
	// MySQLだと文字コードの問題で"?parseTime=true"を末尾につける必要がある
	CONNECT := USER + ":" + PASS + "@/" + DBNAME + "?parseTime=true"
	db, err := gorm.Open(DBMS, CONNECT)

	if err != nil {
		panic(err.Error())
	}
	return db
}

// DB initialization
func DbInit() {
	db := gormConnect()
	// コネクション解放
	defer db.Close()
	db.AutoMigrate(&User{})
}

// PasswordEncrypt パスワードをhash化
func PasswordEncrypt(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(hash), err
}

// CompareHashAndPassword hashと非hashパスワード比較
func CompareHashAndPassword(hash, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
}

// create user data
func createUser(username string, password string) []error {
	passwordEncrypt, _ := PasswordEncrypt(password)
	db := gormConnect()
	defer db.Close()
	// Insert処理
	if err := db.Create(&User{Username: username, Password: passwordEncrypt}).GetErrors(); err != nil {
		return err
	}
	return nil
}

// ユーザーを一件取得
func getUser(username string) User {
	db := gormConnect()
	var user User
	db.First(&user, "username = ?", username)
	db.Close()
	return user
}

// sing up
func Signup(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")
	// 登録ユーザーが重複していた場合にはじく処理
	if err := createUser(username, password); len(err) != 0 {
		log.Println("重複したユーザー名です")
		c.HTML(http.StatusBadRequest, "signup.html", gin.H{"err": err})
	}
	c.Redirect(302, "/signin")
}

// sing in
func Signin(c *gin.Context) {
	// DBから取得したユーザーパスワード(Hash)
	dbPassword := getUser(c.PostForm("username")).Password
	log.Println(dbPassword)
	// フォームから取得したユーザーパスワード
	formPassword := c.PostForm("password")

	// ユーザーパスワードの比較
	if err := CompareHashAndPassword(dbPassword, formPassword); err != nil {
		log.Println(err)
		log.Println("ログインできませんでした")
		c.HTML(http.StatusBadRequest, "signin.html", gin.H{"err": err})
		c.Abort()
	} else {
		log.Println("ログインできました")
		c.Redirect(302, "/home")
	}
}
