package q

import (
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

type Q struct {
	gorm.Model
	Title   string
	Content string
}

func gormConnect() *gorm.DB {
	DBMS := "mysql"
	USER := "koki"
	PASS := "mayomayo"
	DBNAME := "q"
	// MySQLだと文字コードの問題で"?parseTime=true"を末尾につける必要がある
	CONNECT := USER + ":" + PASS + "@/" + DBNAME + "?parseTime=true"
	db, err := gorm.Open(DBMS, CONNECT)

	if err != nil {
		panic(err.Error())
	}
	return db
}

// DB initialization
func DbInit() {
	db := gormConnect()
	// コネクション解放
	defer db.Close()
	db.AutoMigrate(&Q{})
}

// create user data
func createQ(title string, content string) []error {
	db := gormConnect()
	defer db.Close()
	// Insert処理
	if err := db.Create(&Q{Title: title, Content: content}).GetErrors(); err != nil {
		return err
	}
	return nil
}

func FindQ(keyword string) []Q {
	db := gormConnect()
	qs := []Q{}
	db.Find(&qs, "content like ?", "%"+keyword+"%")
	db.Close()
	return qs
}

// 検索条件に本文が一致するqを複数取得
func FetchQ(c *gin.Context) {
	resultQ := FindQ(c.Query("keyword"))
    // URLへのアクセスに対してJSONを返す
    c.JSON(200, resultQ)
}

// 
func Ask(c *gin.Context) {
	title := c.PostForm("title")
	content := c.PostForm("content")
	createQ(title,content)
	c.Redirect(302, "/home")
}
